package youtubedl

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os/exec"
	"strings"
	"sync"
	"time"
)

type DownloadOptions struct {
	Bandwidth string
}

type jsonVideoFormat struct {
	ID string `json:"format_id"`
	Name string `json:"format"`
	Extension string `json:"ext"`
	AudioCodec string `json:"acodec"`
	AudioBitrate float64 `json:"abr"`
	AudioSampleRate uint64 `json:"asr"`
	FileSize uint64 `json:"filesize"`
	FrameRate float64 `json:"fps"`
	VideoCodec string `json:"vcodec"`
	VideoWidth uint64 `json:"width"`
	VideoHeight uint64 `json:"height"`
	TotalBitRate float64 `json:"tbr"`
	ContainerFormat string `json:"container"`
}

type jsonVideoInfo struct {
	ID string `json:"id"`
	Title string `json:"title"`
	Description string `json:"description"`
	UploadDate string `json:"upload_date"`
	Formats []*jsonVideoFormat `json:"formats"`
}

type VideoResolution string
const (
	NoVideoResolution = VideoResolution("")
	Res144p = VideoResolution("144p")
	Res240p = VideoResolution("240p")
	Res270p = VideoResolution("270p")
	Res360p = VideoResolution("360p")
	Res480p = VideoResolution("480p")
	Res720p = VideoResolution("720p")
	Res1080p = VideoResolution("1080p")
	Res1440p = VideoResolution("1440p")
	Res2160p = VideoResolution("2160p")
	Res3072p = VideoResolution("3072p")
)

type VideoCodec string
const (
	NoVideoCodec = VideoCodec("")
	AVC1 = VideoCodec("AVC1")
	H265 = VideoCodec("H.265")
	VP9 = VideoCodec("VP9")
	H264 = VideoCodec("H.264")
)

type AudioCodec string
const (
	NoAudioCodec = AudioCodec("")
	MP4A = AudioCodec("MP4A")
	Opus = AudioCodec("Opus")
	MP3 = AudioCodec("MP3")
	FLAC = AudioCodec("FLAC")
)

type Format struct {
	Name string
	IsVideo bool
	IsAudio bool
	data *jsonVideoFormat
}

func newFormat(data *jsonVideoFormat) *Format {
	return &Format{
		Name: data.Name,
		IsVideo: data.VideoCodec != "" && data.VideoCodec != "none",
		IsAudio: data.AudioCodec != "" && data.AudioCodec != "none",
		data: data,
	}
}

func (f *Format) ID() string {
	return f.data.ID
}

func (f *Format) String() string {
	return f.Name
}

func (f *Format) VideoResolution() VideoResolution {
	h := f.data.VideoHeight
	if !f.IsVideo || h == 0 {
		return NoVideoResolution
	}
	switch h {
		case 144: return Res144p
		case 240: return Res240p
		case 270: return Res270p
		case 360: return Res360p
		case 480: return Res480p
		case 720: return Res720p
		case 1080: return Res1080p
		case 1440: return Res1440p
		case 2160: return Res2160p
		case 3072: return Res3072p
	}
	return VideoResolution(fmt.Sprintf("%dp", h))
}

func (f *Format) VideoCodec() VideoCodec {
	if !f.IsVideo {
		return NoVideoCodec
	}
	c := strings.ToLower(f.data.VideoCodec)
	if strings.Contains(c, "avc") {
		return AVC1
	}
	if strings.Contains(c, "vp9") {
		return VP9
	}
	if strings.Contains(c, "h265") || strings.Contains(c, "h.265") {
		return H265
	}
	if strings.Contains(c, "h264") || strings.Contains(c, "h.264") {
		return H264
	}
	return VideoCodec(c)
}

func (f *Format) AudioCodec() AudioCodec {
	if !f.IsAudio {
		return NoAudioCodec
	}
	c := strings.ToLower(f.data.AudioCodec)
	if strings.Contains(c, "flac") {
		return FLAC
	}
	if strings.Contains(c, "opus") {
		return Opus
	}
	if strings.Contains(c, "mp4a") {
		return MP4A
	}
	if strings.Contains(c, "mp3") {
		return MP3
	}
	return AudioCodec(c)
}

// In bits per second.
func (f *Format) AudioBitrate() float64 {
	return f.data.AudioBitrate * 1024
}

type FormatSpec struct {
	Single, Audio, Video *Format
}

// NewFormatSpec creates a FormatSpec from a video and audio format.
// If a format provides both video and audio, pass it twice (as both arguments).
func NewFormatSpec(videoFormat, audioFormat *Format) (*FormatSpec, error) {
	if !videoFormat.IsVideo {
		return nil, fmt.Errorf("format %v passed in as video format but actually has no video", videoFormat)
	}
	if !audioFormat.IsAudio {
		return nil, fmt.Errorf("format %v passed in as audio format but actually has no audio", audioFormat)
	}
	if videoFormat.Name == audioFormat.Name {
		return &FormatSpec{Single: videoFormat}, nil
	}
	return &FormatSpec{Video: videoFormat, Audio: audioFormat}, nil
}

func (fs *FormatSpec) Args() []string {
	if fs.Single != nil {
		return []string{"--format", fs.Single.ID()}
	}
	return []string{
		"--format", fmt.Sprintf("%s+%s", fs.Video.ID(), fs.Audio.ID()),
	}
}

var (
	UnavailableError = errors.New("video is unavailable")
	RequiresPaymentError = errors.New("requires payment")
)

func parseError(err error) error {
	if err == nil {
		return nil
	}
	var exitError *exec.ExitError
	if errors.As(err, &exitError) {
		stderr := string(exitError.Stderr)
		if strings.Contains(stderr, "Video unavailable") {
			return UnavailableError
		}
		if strings.Contains(stderr, "This video requires payment") {
			return RequiresPaymentError
		}
	}
	return err
}

type Video struct {
	URL string
	metaMu sync.Mutex
	formats map[string]*Format
	uploadDate time.Time
	rawJSON []byte
	data *jsonVideoInfo
}

func NewVideo(url string) *Video {
	return &Video{URL: url}
}

func (v *Video) loadMeta(ctx context.Context) error {
	v.metaMu.Lock()
	defer v.metaMu.Unlock()
	if v.data != nil {
		return nil
	}
	cmd := exec.CommandContext(
		ctx,
		"youtube-dl",
		"--no-call-home",
		"--geo-bypass",
		"--dump-single-json",
		v.URL)
	output, err := cmd.Output()
	if err != nil {
		err = parseError(err)
		if exitErr, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("[cmd: %v] cannot get video data for %v: %v [stderr: %v]", cmd, v.URL, err, string(exitErr.Stderr))
		}
		return fmt.Errorf("cannot get video data for %v: %w", v.URL, err)
	}
	data := &jsonVideoInfo{}
	if err := json.Unmarshal(output, data); err != nil {
		return fmt.Errorf("cannot unmarshal json: %w", err)
	}
	formats := make(map[string]*Format, len(data.Formats))
	for _, f := range data.Formats {
		format := newFormat(f)
		formats[format.Name] = format
	}
	uploadDate, err := time.Parse("20060102", data.UploadDate)
	if err != nil {
		return fmt.Errorf("cannot parse upload date: %w", err)
	}
	v.rawJSON = output
	v.data = data
	v.formats = formats
	v.uploadDate = uploadDate
	return nil
}

func (v *Video) Title(ctx context.Context) (string, error) {
	if err := v.loadMeta(ctx); err != nil {
		return "", err
	}
	return v.data.Title, nil
}

func (v *Video) Description(ctx context.Context) (string, error) {
	if err := v.loadMeta(ctx); err != nil {
		return "", err
	}
	return v.data.Description, nil
}

func (v *Video) Published(ctx context.Context) (time.Time, error) {
	if err := v.loadMeta(ctx); err != nil {
		return time.Time{}, err
	}
	return v.uploadDate, nil
}

func (v *Video) Formats(ctx context.Context) ([]*Format, error) {
	if err := v.loadMeta(ctx); err != nil {
		return nil, err
	}
	formats := make([]*Format, 0, len(v.formats))
	for _, format := range v.formats {
		formats = append(formats, format)
	}
	return formats, nil
}

func (v *Video) JSONData(ctx context.Context) ([]byte, error) {
	if err := v.loadMeta(ctx); err != nil {
		return nil, err
	}
	return v.rawJSON, nil
}

func (v *Video) Extension() string {
	return "mkv"
}

func (v *Video) Download(ctx context.Context, spec *FormatSpec, downloadOptions *DownloadOptions, outputFile string) error {
	args := []string{
		"--no-call-home",
		"--geo-bypass",
		"--merge-output-format", "mkv",
		"--all-subs",
		"--sub-format", "best",
		"--embed-subs",
		"--convert-subs", "srt",
		"--add-metadata",
		"--fixup", "detect_or_warn",
		"--no-part",
		"--no-cache-dir",
		"--output", strings.ReplaceAll(outputFile, "%", "%%"),
	}
	if downloadOptions != nil {
		if downloadOptions.Bandwidth != "" {
			args = append(args, "--limit-rate", downloadOptions.Bandwidth)
		}
	}
	args = append(args, spec.Args()...)
	args = append(args, v.URL)
	cmd := exec.CommandContext(ctx, "youtube-dl", args...)
	_, err := cmd.Output()
	if err != nil {
		err = parseError(err)
		if exitErr, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("[cmd: %v] cannot dowmload: %v [stderr: %v]", cmd, err, string(exitErr.Stderr))
		}
	}
	return err
}

type jsonVideoListEntry struct {
	ID string    `json:"id"`
	Title string `json:"title"`
}

type jsonVideoList struct {
	Entries []*jsonVideoListEntry `json:"entries"`
}

type PlaylistEntry struct {
	ID string
	Title string
}

func GetPlaylistVideos(ctx context.Context, playlistURL string) ([]*PlaylistEntry, error) {
	cmd := exec.CommandContext(
		ctx,
		"youtube-dl",
		"--no-call-home",
		"--geo-bypass",
		"--flat-playlist",
		"--dump-single-json",
		playlistURL)
	output, err := cmd.Output()
	if err != nil {
		return nil, fmt.Errorf("cannot get videos in playlist %v: %w", playlistURL, err)
	}
	data := &jsonVideoList{}
	if err := json.Unmarshal(output, data); err != nil {
		return nil, fmt.Errorf("cannot unmarshal json: %w", err)
	}
	playlistEntries := make([]*PlaylistEntry, 0, len(data.Entries))
	for _, entry := range data.Entries {
		playlistEntries = append(playlistEntries, &PlaylistEntry{
			ID: entry.ID,
			Title: entry.Title,
		})
	}
	return playlistEntries, nil
}
