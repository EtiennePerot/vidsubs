package filter

import (
	"fmt"
	"regexp"
	"time"

	"perot.me/vidsubs/sub"
	"perot.me/vidsubs/yamlcfg"
)

type Filter struct {
	invert     bool
	titleRegex *regexp.Regexp
	mustWatch  *bool
	maxOld     *time.Duration
	tag        string
	and        []*Filter
	or         []*Filter
}

func New(yamlCfg *yamlcfg.Filter) (*Filter, error) {
	if yamlCfg == nil {
		return nil, nil
	}
	var titleRegex *regexp.Regexp
	if yamlCfg.TitleRegex != nil {
		var err error
		titleRegex, err = regexp.Compile("(?i)" + *yamlCfg.TitleRegex)
		if err != nil {
			return nil, fmt.Errorf("invalid regular expression: %w", err)
		}
	}
	var maxOld *time.Duration
	if yamlCfg.MaxDaysOld != nil {
		maxOldDuration := time.Duration(*yamlCfg.MaxDaysOld) * 24*time.Hour
		maxOld = &maxOldDuration
	}
	var tag string
	if yamlCfg.Tag != nil {
		tag = *yamlCfg.Tag
	}
	var and, or []*Filter
	if numAnd := len(yamlCfg.And); numAnd > 0 {
		and = make([]*Filter, 0, numAnd)
		for i, yf := range yamlCfg.And {
			f, err := New(yf)
			if err != nil {
				return nil, fmt.Errorf("and[%d]: %w", i, err)
			}
			and = append(and, f)
		}
	}
	if numOr := len(yamlCfg.Or); numOr > 0 {
		or = make([]*Filter, 0, numOr)
		for i, yf := range yamlCfg.Or {
			f, err := New(yf)
			if err != nil {
				return nil, fmt.Errorf("or[%d]: %w", i, err)
			}
			or = append(or, f)
		}
	}
	return &Filter{
		invert:     yamlCfg.Invert != nil && *yamlCfg.Invert,
		titleRegex: titleRegex,
		mustWatch:  yamlCfg.MustWatch,
		maxOld:     maxOld,
		tag:        tag,
		and:        and,
		or:         and,
	}, nil
}

func (f *Filter) maybeInvert(result bool) bool {
	if f.invert {
		return !result
	}
	return result
}

func (f *Filter) Filter(video sub.Video, meta *sub.VideoMeta) bool {
	if f == nil {
		return true
	}
	channel := video.Channel()
	if f.titleRegex != nil && !f.titleRegex.MatchString(meta.Title) {
		return f.maybeInvert(false)
	}
	if f.mustWatch != nil && *f.mustWatch != channel.MustWatch() {
		return f.maybeInvert(false)
	}
	if f.maxOld != nil && time.Since(meta.Published) > *f.maxOld {
		return f.maybeInvert(false)
	}
	if f.tag != "" {
		foundByTag := false
		for _, t := range channel.Tags() {
			if t == f.tag {
				foundByTag = true
				break
			}
		}
		if !foundByTag {
			return f.maybeInvert(false)
		}
	}
	for _, and := range f.and {
		if !and.Filter(video, meta) {
			return f.maybeInvert(false)
		}
	}
	if len(f.or) > 0 {
		foundByOr := false
		for _, or := range f.or {
			if or.Filter(video, meta) {
				foundByOr = true
				break
			}
		}
		if !foundByOr {
			return f.maybeInvert(false)
		}
	}
	return f.maybeInvert(true)
}
