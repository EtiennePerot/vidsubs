package updater

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"perot.me/vidsubs/config"
	"perot.me/vidsubs/sub"
)

type Updater struct {
	cfg *config.Config
	lastCacheSave time.Time
}

type videoToDownload struct {
	video sub.Video
	qualityPolicy sub.QualityPolicy
	meta *sub.VideoMeta
}

func (vtd *videoToDownload) String() string {
	s := fmt.Sprintf("%v", vtd.video)
	if vtd.meta != nil {
		s += fmt.Sprintf(" (published: %s)", vtd.meta.Published.Format("2006-01-02"))
	}
	return s
}

func relativeSymlink(outputFile, symlinkPath string) error {
	absOutputFile, err := filepath.Abs(outputFile)
	if err != nil {
		return fmt.Errorf("cannot make output file %q absolute: %w", outputFile, err)
	}
	absSymlinkPath, err := filepath.Abs(symlinkPath)
	if err != nil {
		return fmt.Errorf("cannot make symlink path %q absolute: %w", symlinkPath, err)
	}
	absSymlinkDir := filepath.Dir(absSymlinkPath)
	symlinkTarget, err := filepath.Rel(absSymlinkDir, absOutputFile)
	if err != nil {
		return fmt.Errorf("cannot compute relative path relationship from output file %q to symlink dir path %q: %w", absOutputFile, absSymlinkDir, err)
	}
	// Check if there's already a valid symlink for this.
	lstat, err := os.Lstat(absSymlinkPath)
	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("cannot verify if symlink %q already exists: %w", absSymlinkPath, err)
	} else if err == nil {
		if lstat.Mode() & os.ModeSymlink == 0 {
			return fmt.Errorf("path %q already exists and is not a symlink: %v", absSymlinkPath, lstat.Mode())
		}
		existingTarget, err := os.Readlink(absSymlinkPath)
		if err != nil {
			return fmt.Errorf("cannot read target of symlink %q: %w", absSymlinkPath, err)
		}
		if existingTarget == symlinkTarget {
			// Symlink is already up-to-date.
			return nil
		}
		if err := os.Remove(absSymlinkPath); err != nil {
			return fmt.Errorf("cannot remove existing symlink %q: %w", absSymlinkPath, err)
		}
		// Old symlink removed, re-created outside of this branch.
	}
	return os.Symlink(symlinkTarget, absSymlinkPath)
}

func (u *Updater) downloadVideo(ctx context.Context, vtd *videoToDownload) error {
	outputFile, err := u.cfg.StoragePath(vtd.video, vtd.meta)
	if err != nil {
		return fmt.Errorf("cannot evaluate where to store the video: %w", err)
	}
	outputDir := filepath.Dir(outputFile)
	if err := os.MkdirAll(outputDir, u.cfg.StorageInfo.Mode()); err != nil {
		return fmt.Errorf("cannot prepare output directory %q: %w", outputDir, err)
	}
	if err := u.cfg.ChownToConfig(outputDir); err != nil {
		return fmt.Errorf("cannot chown output directory %q: %w", outputDir, err)
	}
	if err := vtd.video.Download(ctx, u.cfg.DownloadOptions, vtd.qualityPolicy, outputFile); err != nil {
		return fmt.Errorf("cannot download: %w", err)
	}
	if err := os.Chmod(outputFile, u.cfg.StorageInfo.Mode().Perm()); err != nil {
		return fmt.Errorf("cannot adjust file permissions on %q: %w", outputFile, err)
	}
	if err := u.cfg.ChownToConfig(outputFile); err != nil {
		return fmt.Errorf("cannot chown output file %q: %w", outputFile, err)
	}
	if err := os.Chtimes(outputFile, vtd.meta.Published, vtd.meta.Published); err != nil {
		return fmt.Errorf("cannot adjust file time on %q: %w", outputFile, err)
	}
	for _, sl := range u.cfg.SymlinkLayouts {
		symlinkPaths, err := sl.SymlinkPaths(vtd.video, vtd.meta)
		if err != nil {
			return fmt.Errorf("cannot get list of intended symlink paths for %v: %w", sl.Name, err)
		}
		for _, symlinkPath := range symlinkPaths {
			symlinkDir := filepath.Dir(symlinkPath)
			if err := os.MkdirAll(symlinkDir, u.cfg.StorageInfo.Mode()); err != nil {
				return fmt.Errorf("cannot prepare symlink directory %q: %w", symlinkDir, err)
			}
			if err := u.cfg.ChownToConfig(symlinkDir); err != nil {
				return fmt.Errorf("cannot chown symlink directory %q: %w", symlinkDir, err)
			}
			if err := relativeSymlink(outputFile, symlinkPath); err != nil {
				return fmt.Errorf("cannot create symlink from %q to %q: %w", symlinkPath, outputFile, err)
			}
			// No chmodding of the symlink; symlinks are all 777.
			if err := u.cfg.ChownToConfig(symlinkPath); err != nil {
				return fmt.Errorf("cannot chown symlink %q: %w", symlinkPath, err)
			}
			if err := os.Chtimes(symlinkPath, vtd.meta.Published, vtd.meta.Published); err != nil {
				return fmt.Errorf("cannot adjust file time on %q: %w", symlinkPath, err)
			}
		}
	}
	return nil
}

// Don't consider videos newer than this to ensure they have all been converted to all formats.
const minVideoAge = 2 * time.Hour

// Don't save the cache to disk more often than once per this interval.
const intervalBetweenCacheSaves = 1 * time.Hour

func (u *Updater) maybeSaveCache(ctx context.Context) error {
	if u.lastCacheSave.IsZero() {
		u.lastCacheSave = time.Now()
		return nil
	}
	if time.Since(u.lastCacheSave) < intervalBetweenCacheSaves {
		return nil
	}
	return u.cfg.Cache.Save(ctx)
}

func (u *Updater) Update(ctx context.Context) error {
	var allVideos []*videoToDownload
	cutoff := time.Now().Add(-minVideoAge)
	var errs []error
	logError := func(format string, values ...interface{}) {
		err := fmt.Errorf(format, values...)
		fmt.Printf("Non-fatal error (continuing anyway): %v\n", err)
		errs = append(errs, err)
	}
	maybeSaveCache := func() {
		if err := u.maybeSaveCache(ctx); err != nil {
			logError("cannot save cache: %w", err)
		}
	}
	rnd := rand.New(rand.NewSource(time.Now().Unix()))
	fmt.Print("Updater started.\n")
	numChannels := len(u.cfg.Channels)
	lastSeenIDFromChannels := u.cfg.Cache.LastSeenIDFromChannels()
	for i, channel := range u.cfg.Channels {
		if !channel.Active() {
			continue
		}
		lastSeenIDFromChannel := lastSeenIDFromChannels.LastSeen(channel.ID())
		if lastSeenIDFromChannel == "" {
			fmt.Printf("Getting list of videos from channel %d/%d: %s (initial fetch)\n", i+1, numChannels, channel.Name())
		} else if rnd.Intn(100) == 42 {
			// With some probability, ignore last-seen ID and re-fetch all videos.
			fmt.Printf("Getting list of videos from channel %d/%d: %s (ignoring last seen ID: %v)\n", i+1, numChannels, channel.Name(), lastSeenIDFromChannel)
			lastSeenIDFromChannel = ""
		} else {
			fmt.Printf("Getting list of videos from channel %d/%d: %s (last seen ID: %v)\n", i+1, numChannels, channel.Name(), lastSeenIDFromChannel)
		}
		channelVideos, err := channel.Videos(ctx, lastSeenIDFromChannel)
		if err != nil {
			logError("cannot get videos from %s: %w", channel.Name(), err)
			continue
		}
		qualityPolicy := channel.QualityPolicy()
		for _, channelVideo := range channelVideos {
			allVideos = append(allVideos, &videoToDownload{
				video: channelVideo,
				qualityPolicy: qualityPolicy,
			})
		}
	}
	fmt.Printf("%d total videos. Checking for already-skipped and already-downloaded videos...\n", len(allVideos))
	skippedIDs := u.cfg.Cache.SkippedIDs()
	toDownload := make(map[sub.VideoID]*videoToDownload, len(allVideos))
	var partialFilesToDelete []string
	for _, vtd := range allVideos {
		vid := vtd.video.ID()
		if reason := skippedIDs.ShouldSkip(vid); reason != "" {
			continue
		}
		toDownload[vid] = vtd
	}
	if err := filepath.Walk(u.cfg.StorageDir, func(path string, info os.FileInfo, err error) error {
		if ctx.Err() != nil {
			return ctx.Err()
		}
		if !info.Mode().IsRegular() {
			return nil
		}
		for videoID := range toDownload {
			if strings.Contains(path, string(videoID)) {
				delete(toDownload, videoID)
			}
		}
		basePath := filepath.Base(path)
		if len(basePath) > 2 && strings.HasPrefix(basePath, ".") {
			absPath, err := filepath.Abs(path)
			if err != nil {
				logError("Warning: cannot clean up possibly-partial file %v: cannot make the path absolute: %w\n", path, err)
			} else {
				partialFilesToDelete = append(partialFilesToDelete, absPath)
			}
		}
		return nil
	}); err != nil {
		return fmt.Errorf("cannot verify already-downloaded videos: %w", err)
	}
	numToDownload := len(toDownload)
	fmt.Printf("%d videos to download. Obtaining metadata...\n", numToDownload)
	gotErrorFromChannel := make(map[sub.ChannelID]bool, len(u.cfg.Channels))
	videosWithMeta := make([]*videoToDownload, 0, len(toDownload))
	i := 0
	for _, vtd := range toDownload {
		i++
		fmt.Printf("Obtaining metadata for video %d/%d: %v\n", i, numToDownload, vtd)
		meta, err := vtd.video.Meta(ctx)
		if err != nil {
			gotErrorFromChannel[vtd.video.Channel().ID()] = true
			var videoErr *sub.VideoError
			if errors.As(err, &videoErr) && videoErr.IsPermanentReason != "" {
				logError("permanently cannot get information for %v: %w (permanent: %v)", vtd, err, videoErr.IsPermanentReason)
				skippedIDs.Add(vtd.video.ID(), videoErr.IsPermanentReason)
				maybeSaveCache()
			} else {
				logError("cannot get information for %v: %w", vtd, err)
			}
			continue
		}
		vtd.meta = meta
		videosWithMeta = append(videosWithMeta, vtd)
	}
	numWithMeta := len(videosWithMeta)
	fmt.Printf("%d videos with metadata obtained. Applying filters and sorting videos...\n", numWithMeta)
	downloadOrder := make([]*videoToDownload, 0, numWithMeta)
	for _, vtd := range videosWithMeta {
		if vtd.meta.Published.After(cutoff) {
			fmt.Printf("Skipping video %d/%d (%v) as it is too recent.\n", i, numToDownload, vtd)
			continue // Video too new.
		}
		if vtd.video.Channel().Filter()(vtd.video, vtd.meta) {
			downloadOrder = append(downloadOrder, vtd)
		} else {
			fmt.Printf("Skipping video (not matching filters): %v\n", vtd)
			skippedIDs.Add(vtd.video.ID(), "not matching filters")
			maybeSaveCache()
		}
	}
	sort.Slice(downloadOrder, func(i, j int) bool {
		x, y := downloadOrder[i], downloadOrder[j]
		xCh, yCh := x.video.Channel(), y.video.Channel()
		xMustWatch, yMustWatch := xCh.MustWatch(), yCh.MustWatch()
		if xMustWatch != yMustWatch {
			// Must watch first in the list (i.e. "less").
			return xMustWatch
		}
		xPublished, yPublished := x.meta.Published, y.meta.Published
		if !xPublished.Equal(yPublished) {
			// Chronological order.
			return xPublished.Before(yPublished)
		}
		xChName, yChName := xCh.Name(), yCh.Name()
		if xChName != yChName {
			return xChName < yChName
		}
		return x.video.ID() < y.video.ID()
	})
	if numToDownload = len(downloadOrder); numToDownload == 0 {
		fmt.Println("No videos to download; all up-to-date.")
	} else {
		i = 0
		fmt.Printf("%d videos to download. Downloading...\n", numToDownload)
		for _, vtd := range downloadOrder {
			i++
			fmt.Printf("Downloading video %d/%d: %v\n", i, numToDownload, vtd)
			if err := u.downloadVideo(ctx, vtd); err != nil {
				var videoErr *sub.VideoError
				if errors.As(err, &videoErr) && videoErr.IsPermanentReason != "" {
					logError("video %v: %w (permanent: %v)", vtd, err, videoErr.IsPermanentReason)
					skippedIDs.Add(vtd.video.ID(), videoErr.IsPermanentReason)
					maybeSaveCache()
				} else {
					logError("video %v: %w", vtd, err)
					gotErrorFromChannel[vtd.video.Channel().ID()] = true
				}
			} else if !gotErrorFromChannel[vtd.video.Channel().ID()] {
				// Only update last-seen video ID if there has not been
				// an error downloading this channel's videos so far.
				// This assumes that we are downloading in
				// chronological order.
				lastSeenIDFromChannels.SetLastSeen(vtd.video.Channel().ID(), vtd.video.ID())
				maybeSaveCache()
			}
		}
	}
	for _, partialFile := range partialFilesToDelete {
		if err := os.Remove(partialFile); err != nil {
			logError("cannot delete possibly-partial file %q: %w", partialFile, err)
		}
	}
	if err := u.cfg.Cache.Save(ctx); err != nil {
		logError("cannot save cache data: %w", err)
	}
	if numErrors := len(errs); numErrors > 0 {
		if numErrors == 1 {
			return errs[0]
		}
		return fmt.Errorf("%d errors: %v", numErrors, errs)
	}
	fmt.Printf("All finished with no errors.\n")
	return nil
}

func New(cfg *config.Config) *Updater {
	return &Updater{cfg: cfg}
}
