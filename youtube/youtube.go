package youtube

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"

	"github.com/mmcdole/gofeed"
	"golang.org/x/sync/errgroup"
	"perot.me/vidsubs/filter"
	"perot.me/vidsubs/mkvmerge"
	"perot.me/vidsubs/sub"
	"perot.me/vidsubs/yamlcfg"
	"perot.me/vidsubs/youtubedl"
)

func makeID(youtubeID string) sub.VideoID {
	return sub.VideoID(fmt.Sprintf("yt=%s", youtubeID))
}

type video struct {
	*sub.BaseVideo
	youtubeID   string
	ytdl        *youtubedl.Video
	channel     *channel
	meta        *sub.VideoMeta
	description string
}

func ytdlErr(err error) error {
	if err == nil {
		return err
	}
	if errors.Is(err, youtubedl.UnavailableError) {
		return sub.NewPermanentVideoError(err, "video is unavailable")
	}
	if errors.Is(err, youtubedl.RequiresPaymentError) {
		return sub.NewPermanentVideoError(err, "video requires payment")
	}
	return err
}

func (v *video) Meta(ctx context.Context) (*sub.VideoMeta, error) {
	if v.meta == nil {
		title, err := v.ytdl.Title(ctx)
		err = ytdlErr(err)
		if err != nil {
			return nil, fmt.Errorf("cannot get title: %w", err)
		}
		v.BaseVideo.SetKnownTitle(title)
		videoURL := fmt.Sprintf("https://www.youtube.com/watch?v=%s", v.youtubeID)
		description, err := v.ytdl.Description(ctx)
		err = ytdlErr(err)
		if err != nil {
			return nil, fmt.Errorf("cannot get description: %w", err)
		}
		v.description = fmt.Sprintf("%s\n%s\n\n%s", title, videoURL, description)
		published, err := v.ytdl.Published(ctx)
		err = ytdlErr(err)
		if err != nil {
			return nil, fmt.Errorf("cannot get publication date: %w", err)
		}
		v.meta = &sub.VideoMeta{
			Title:     title,
			Published: published,
			Extension: v.ytdl.Extension(),
		}
	}
	return v.meta, nil
}

var resolutionToScore = map[sub.QualityPolicy]map[youtubedl.VideoResolution]int{
	sub.QualityStandard: map[youtubedl.VideoResolution]int{
		youtubedl.Res144p: -4,
		youtubedl.Res240p: -3,
		youtubedl.Res270p: -2,
		youtubedl.Res360p: -1,
		youtubedl.Res480p: 0,
		youtubedl.Res720p: 1,
		youtubedl.Res2160p: 2,
		youtubedl.Res3072p: 3,
		youtubedl.Res1080p: 4,
		youtubedl.Res1440p: 5,
	},
	sub.QualityBest: map[youtubedl.VideoResolution]int{
		youtubedl.Res144p: -4,
		youtubedl.Res240p: -3,
		youtubedl.Res270p: -2,
		youtubedl.Res360p: -1,
		youtubedl.Res480p: 0,
		youtubedl.Res720p: 1,
		youtubedl.Res1080p: 2,
		youtubedl.Res1440p: 3,
		youtubedl.Res2160p: 4,
		youtubedl.Res3072p: 5,
	},
	sub.QualityTrash: map[youtubedl.VideoResolution]int{
		youtubedl.Res3072p: -5,
		youtubedl.Res2160p: -4,
		youtubedl.Res1440p: -3,
		youtubedl.Res144p: -2,
		youtubedl.Res270p: -1,
		youtubedl.Res240p: 0,
		youtubedl.Res360p: 1,
		youtubedl.Res1080p: 2,
		youtubedl.Res720p: 3,
		youtubedl.Res480p: 4,
	},
	sub.QualityMedium: map[youtubedl.VideoResolution]int{
		youtubedl.Res3072p: -5,
		youtubedl.Res2160p: -4,
		youtubedl.Res1440p: -3,
		youtubedl.Res144p: -2,
		youtubedl.Res270p: -1,
		youtubedl.Res240p: 0,
		youtubedl.Res360p: 1,
		youtubedl.Res1080p: 2,
		youtubedl.Res480p: 3,
		youtubedl.Res720p: 4,
	},
	sub.QualityWorst: map[youtubedl.VideoResolution]int{
		youtubedl.Res144p: 3,
		youtubedl.Res240p: 2,
		youtubedl.Res270p: 1,
		youtubedl.Res360p: 0,
		youtubedl.Res480p: -1,
		youtubedl.Res720p: -2,
		youtubedl.Res1080p: -3,
		youtubedl.Res1440p: -4,
		youtubedl.Res2160p: -5,
		youtubedl.Res3072p: -6,
	},
}

var videoCodecToScore = map[youtubedl.VideoCodec]int{
	youtubedl.H264: 1,
	youtubedl.VP9: 2,
	youtubedl.H265: 3,
	youtubedl.AVC1: 4,
}

var audioCodecToScore = map[youtubedl.AudioCodec]int{
	youtubedl.MP3: 1,
	youtubedl.MP4A: 2,
	youtubedl.Opus: 3,
	youtubedl.FLAC: 4,
}

func preferVideoFormat(qualityPolicy sub.QualityPolicy, oldFormat, newFormat *youtubedl.Format) bool {
	oldScore := resolutionToScore[qualityPolicy][oldFormat.VideoResolution()]
	newScore := resolutionToScore[qualityPolicy][newFormat.VideoResolution()]
	if oldScore != newScore {
		return newScore > oldScore
	}
	oldScore = videoCodecToScore[oldFormat.VideoCodec()]
	newScore = videoCodecToScore[newFormat.VideoCodec()]
	return newScore >= oldScore
}

func preferAudioFormat(qualityPolicy sub.QualityPolicy, oldFormat, newFormat *youtubedl.Format) bool {
	oldScore := audioCodecToScore[oldFormat.AudioCodec()]
	newScore := audioCodecToScore[newFormat.AudioCodec()]
	if oldScore != newScore {
		return newScore > oldScore
	}
	return newFormat.AudioBitrate() >= oldFormat.AudioBitrate()
}

func getTempFileNextTo(outputFile, extension string) string {
	return filepath.Join(filepath.Dir(outputFile), "."+filepath.Base(outputFile)+"."+extension)
}

func (v *video) download(ctx context.Context, fileType string, u *url.URL, outputFile string) error {
	output, err := os.Create(outputFile)
	if err != nil {
		return fmt.Errorf("cannot create temporary %s file %q: %w", fileType, outputFile, err)
	}
	defer output.Close()
	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return fmt.Errorf("cannot create HTTP request for %s %q: %w", fileType, u, err)
	}
	req = req.WithContext(ctx)
	resp, err := v.channel.common.httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("cannot perform HTTP request for %s %q: %w", fileType, u, err)
	}
	defer resp.Body.Close()
	if written, err := io.Copy(output, resp.Body); err != nil {
		return fmt.Errorf("%s download failed after downloading %d bytes: %w", fileType, written, err)
	}
	return nil
}

func (v *video) Download(ctx context.Context, downloadOptions *sub.DownloadOptions, qualityPolicy sub.QualityPolicy, outputFile string) error {
	formats, err := v.ytdl.Formats(ctx)
	err = ytdlErr(err)
	if err != nil {
		return fmt.Errorf("cannot get video formats: %w", err)
	}
	var videoFormat, audioFormat *youtubedl.Format
	var foundVideo, foundAudio bool
	for _, format := range formats {
		if format.IsVideo && (!foundVideo || preferVideoFormat(qualityPolicy, videoFormat, format)) {
			foundVideo = true
			videoFormat = format
		}
		if format.IsAudio && (!foundAudio || preferAudioFormat(qualityPolicy, audioFormat, format)) {
			foundAudio = true
			audioFormat = format
		}
	}
	if !foundVideo {
		return errors.New("cannot find appropriate video format")
	}
	if !foundAudio {
		return errors.New("cannot find appropriate audio format")
	}
	formatSpec, err := youtubedl.NewFormatSpec(videoFormat, audioFormat)
	if err != nil {
		return fmt.Errorf("cannot build format spec: %w", err)
	}
	tempVideoFile := getTempFileNextTo(outputFile, "tmp.mkv")
	tempIDFile := getTempFileNextTo(outputFile, "id.txt")
	tempDescriptionFile := getTempFileNextTo(outputFile, "description.txt")
	tempJSONDumpFile := getTempFileNextTo(outputFile, "info.json")
	cleanup := func() {
		for _, f := range []string{
			tempVideoFile,
			tempIDFile,
			tempDescriptionFile,
			tempJSONDumpFile,
		} {
			os.Remove(f)
		}
	}
	cleanup() // Cleanup from previous runs.
	defer cleanup()
	ytdlOptions := &youtubedl.DownloadOptions{
		Bandwidth: downloadOptions.Bandwidth,
	}

	group, groupCtx := errgroup.WithContext(ctx)
	group.Go(func() error {
		if err := ioutil.WriteFile(tempIDFile, []byte(v.ID()), 0600); err != nil {
			return fmt.Errorf("cannot write video ID to %q: %w", tempIDFile, err)
		}
		return nil
	})
	group.Go(func() error {
		if err := ioutil.WriteFile(tempDescriptionFile, []byte(v.description), 0600); err != nil {
			return fmt.Errorf("cannot write description to %q: %w", tempDescriptionFile, err)
		}
		return nil
	})
	group.Go(func() error {
		jsonData, err := v.ytdl.JSONData(groupCtx)
		err = ytdlErr(err)
		if err != nil {
			return fmt.Errorf("cannot get json data: %w", err)
		}
		if err := ioutil.WriteFile(tempJSONDumpFile, jsonData, 0600); err != nil {
			return fmt.Errorf("cannot write JSON dump to %q: %w", tempJSONDumpFile, err)
		}
		return nil
	})
	group.Go(func() error {
		return ytdlErr(v.ytdl.Download(groupCtx, formatSpec, ytdlOptions, tempVideoFile))
	})
	if err := group.Wait(); err != nil {
		return err
	}
	videoInfo, err := v.channel.common.merger.Identify(ctx, tempVideoFile)
	if err != nil {
		return fmt.Errorf("cannot analyze video stream: %w", err)
	}
	mergeInputs := []*mkvmerge.Input{{File: videoInfo}}
	attachments := []*mkvmerge.Attachment{
		{
			Name:        "id.txt",
			MIMEType:    "text/plain",
			Description: "YouTube video ID file",
			Path:        tempIDFile,
		},
		{
			Name:        "description.txt",
			MIMEType:    "text/plain",
			Description: "YouTube video description",
			Path:        tempDescriptionFile,
		},
		{
			Name:        "info.json",
			MIMEType:    "application/json",
			Description: "JSON video data dump",
			Path:        tempJSONDumpFile,
		},
	}
	err = v.channel.common.merger.Merge(ctx, v.meta.Title, mergeInputs, attachments, outputFile)
	if err != nil {
		return fmt.Errorf("cannot merge video: %w", err)
	}
	return nil
}

var (
	channelRegexp = regexp.MustCompile("^(?:https?://)?[^/]*youtube\\.com/channel/([^/?&]+)")
	videoRegexp = regexp.MustCompile("^https?://[^/]*youtube\\.com/watch\\?v=([^/?&]+)")
	playlistInitialDataRegexp = regexp.MustCompile("window\\[['\"]ytInitialData['\"]\\]\\s*=\\s*([^\\r\\n]+);[\\r\\n]")
	playlistVideoHTMLRegexp = regexp.MustCompile("<[-\\w]+[^<>]*data-video-ids?=\"([^\"]+)\"[^<>]*>")
)

type common struct {
	httpClient *http.Client
	merger     *mkvmerge.Merger
}

type channel struct {
	*sub.BaseChannel
	common            *common
	uploadsPlaylistID string
	qualityPolicy     sub.QualityPolicy
}

func (ch *channel) newVideo(youtubeID string) *video {
	return &video{
		BaseVideo:   sub.NewBaseVideo(makeID(youtubeID), ch),
		youtubeID:   youtubeID,
		ytdl:        youtubedl.NewVideo(fmt.Sprintf("https://www.youtube.com/watch?v=%s", youtubeID)),
		channel:     ch,
	}
}

type videosResults struct {
	fetcherName string
	isComprehensive bool
	videos []*video
	err error
}

type videosFetcher struct {
	name            string
	fetcher         func(ctx context.Context) ([]*video, error)
	isComprehensive bool
	results         chan<- *videosResults
}

func (f *videosFetcher) fetch(ctx context.Context) {
	videos, err := f.fetcher(ctx)
	f.results <- &videosResults{
		fetcherName: f.name,
		isComprehensive: f.isComprehensive,
		videos: videos,
		err: err,
	}
}

func (ch *channel) newVideoFromFeed(item *gofeed.Item) (*video, error) {
	matches := videoRegexp.FindStringSubmatch(item.Link)
	if len(matches) == 0 {
		return nil, fmt.Errorf("%q does not look like a video URL", item.Link)
	}
	youtubeID := matches[1]
	if item.Title == "" {
		return nil, errors.New("title is empty")
	}
	if item.PublishedParsed == nil {
		return nil, fmt.Errorf("cannot parse publication date %q", item.Published)
	}
	description := item.Description
	if description == "" && item.Custom["media:description"] != "" {
		description = item.Custom["media:description"]
	}
	v := ch.newVideo(youtubeID)
	v.SetKnownTitle(item.Title)
	v.meta = &sub.VideoMeta{
		Title:     item.Title,
		Published: *item.PublishedParsed,
		Extension: "mkv",
	}
	v.description = fmt.Sprintf("%s\n%s\n\n%s", item.Title, item.Link, description)
	return v, nil
}

func (ch *channel) fetchVideosFromRSS(ctx context.Context) ([]*video, error) {
	url := fmt.Sprintf("https://www.youtube.com/feeds/videos.xml?channel_id=%v", ch.ID())
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot create request object: %w", err)
	}
	req = req.WithContext(ctx)
	resp, err := ch.common.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("cannot get channel feed %q: %w", url, err)
	}
	defer resp.Body.Close()
	fp := gofeed.NewParser()
	feed, err := fp.Parse(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("cannot parse feed %q: %w", url, err)
	}
	videos := make([]*video, 0, len(feed.Items))
	for _, item := range feed.Items {
		v, err := ch.newVideoFromFeed(item)
		if err != nil {
			return nil, fmt.Errorf("cannot process feed item %v: %w", item.Title, err)
		}
		videos = append(videos, v)
	}
	return videos, nil
}

type playlistID string

type channelPlaylist struct {
	ch *channel
	id playlistID
}

func (pl *channelPlaylist) url() string {
	return fmt.Sprintf("https://www.youtube.com/playlist?list=%v", pl.id)
}

type playlistInitialDataJSON struct {
	Contents struct {
		TwoColumnBrowseResultsRenderer struct {
			Tabs []struct {
				TabRenderer struct {
					Content struct {
						SectionListRenderer struct {
							Contents []struct {
								ItemSectionRenderer struct {
									Contents []struct {
										PlaylistVideoListRenderer struct {
											Contents []struct {
												PlaylistVideoRenderer struct {
													VideoId string `json:"videoId"`
												} `json:"playlistVideoRenderer"`
											} `json:"contents"`
										} `json:"playlistVideoListRenderer"`
									} `json:"contents"`
								} `json:"itemSectionRenderer"`
							} `json:"contents"`
						} `json:"sectionListRenderer"`
					} `json:"content"`
				} `json: "tabRenderer"`
			} `json:"tabs"`
		} `json:"twoColumnBrowseResultsRenderer"`
	} `json:"contents"`
}

func (pl *channelPlaylist) getDirectly(ctx context.Context) ([]*video, error) {
	playlistURL := pl.url()
	req, err := http.NewRequest("GET", playlistURL, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot create request object: %w", err)
	}
	req = req.WithContext(ctx)
	resp, err := pl.ch.common.httpClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("cannot get channel uploads playlist %q: %w", playlistURL, err)
	}
	defer resp.Body.Close()
	playlistHTML, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("cannot read channel uploads playlist %q: %w", playlistURL, err)
	}
	youtubeIDs := map[string]struct{}{}
	jsonErr := func() error {
		matches := playlistInitialDataRegexp.FindSubmatch(playlistHTML)
		if len(matches) == 0 {
			return fmt.Errorf("cannot find playlist contents in %q", playlistURL)
		}
		playlistData := &playlistInitialDataJSON{}
		if err = json.Unmarshal(matches[1], playlistData); err != nil {
			return fmt.Errorf("cannot process playlist contents in %q: %w", playlistURL, err)
		}
		for _, tab := range playlistData.Contents.TwoColumnBrowseResultsRenderer.Tabs {
			for _, section := range tab.TabRenderer.Content.SectionListRenderer.Contents {
				for _, item := range section.ItemSectionRenderer.Contents {
					for _, l := range item.PlaylistVideoListRenderer.Contents {
						youtubeIDs[l.PlaylistVideoRenderer.VideoId] = struct{}{}
					}
				}
			}
		}
		return nil
	}()
	htmlErr := func() error {
		matches := playlistVideoHTMLRegexp.FindAllSubmatch(playlistHTML, -1)
		if len(matches) == 0 {
			return fmt.Errorf("cannot find playlist contents in %q", playlistURL)
		}
		for _, match := range matches {
			youtubeIDs[string(match[1])] = struct{}{}
		}
		return nil
	}()
	if jsonErr != nil && htmlErr != nil {
		return nil, fmt.Errorf("[by json=%v, by html=%v]", jsonErr, htmlErr)
	}
	videos := make([]*video, 0, len(youtubeIDs))
	for youtubeID := range youtubeIDs {
		videos = append(videos, pl.ch.newVideo(youtubeID))
	}
	return videos, nil
}

func (pl *channelPlaylist) getFromYoutubeDL(ctx context.Context) ([]*video, error) {
	entries, err := youtubedl.GetPlaylistVideos(ctx, pl.url())
	if err != nil {
		return nil, fmt.Errorf("cannot use youtube-dl for playlist retrieval: %w", err)
	}
	videos := make([]*video, 0, len(entries))
	for _, entry := range entries {
		videos = append(videos, pl.ch.newVideo(entry.ID))
	}
	return videos, nil
}

func (ch *channel) Videos(ctx context.Context, lastSeen sub.VideoID) ([]sub.Video, error) {
	var fetchers []*videosFetcher
	fetchResults := make(chan *videosResults)
	fetchers = append(fetchers, &videosFetcher{
		name: "RSS",
		fetcher: ch.fetchVideosFromRSS,
		isComprehensive: false,
		results: fetchResults,
	})
	if ch.uploadsPlaylistID != "" {
		playlist := &channelPlaylist{ch, playlistID(ch.uploadsPlaylistID)}
		fetchers = append(fetchers, &videosFetcher{
			name: "playlist",
			fetcher: playlist.getDirectly,
			 // Direct fetcher isn't comprehensive, because the
			 // initial page contents only contain 100 videos.
			 // If the channel has uploaded more than that, they
			 // are loaded through AJAX, which this fetcher does
			 // not emulate.
			isComprehensive: false,
			results: fetchResults,
		})
		fetchers = append(fetchers, &videosFetcher{
			name: "playlist via youtube-dl",
			fetcher: playlist.getFromYoutubeDL,
			isComprehensive: true,
			results: fetchResults,
		})
	}
	for _, fetcher := range fetchers {
		go fetcher.fetch(ctx)
	}
	var errs []error
	videosMap := make(map[sub.VideoID]*video)
	successfulComprehensive := false
	for range fetchers {
		fetched := <-fetchResults
		if fetched.err != nil {
			errs = append(errs, fmt.Errorf("%v: %v", fetched.fetcherName, fetched.err))
			continue
		}
		for _, video := range fetched.videos {
			existing, alreadyFound := videosMap[video.ID()]
			if alreadyFound && existing.meta != nil {
				continue
			}
			videosMap[video.ID()] = video
		}
		if fetched.isComprehensive {
			successfulComprehensive = true
		}
	}
	numErrs := len(errs)
	if numErrs == len(fetchers) {
		if numErrs == 1 {
			return nil, errs[0]
		}
		return nil, fmt.Errorf("%d errors: %v", numErrs, errs)
	}
	numVideos := len(videosMap)
	if numVideos == 0 {
		if numErrs == 0 {
			return nil, errors.New("channel has no videos")
		}
		if numErrs == 1 {
			return nil, errs[0]
		}
		return nil, fmt.Errorf("found no videos (errors: %v)", errs)
	}
	if lastSeen == "" && !successfulComprehensive {
		return nil, fmt.Errorf("found %d videos, but could not ensure that this is a comprehensive list of videos; errors: %v", numVideos, errs)
	}
	if lastSeen != "" {
		if _, foundLastSeen := videosMap[lastSeen]; !foundLastSeen {
			if successfulComprehensive {
				return nil, fmt.Errorf("found %d videos which is believed to be comprehensive, yet last-seen video ID %q was not found within them; errors: %v", numVideos, lastSeen, errs)
			}
			return nil, fmt.Errorf("found %d videos, but could not find last-seen video ID %q among them; errors: %v", numVideos, lastSeen, errs)
		}
	}
	videos := make([]sub.Video, 0, len(videosMap))
	for _, video := range videosMap {
		videos = append(videos, video)
	}
	return videos, nil
}

func ParseConfig(yamlConfig *yamlcfg.YouTube) ([]sub.Channel, error) {
	merger, err := mkvmerge.New()
	if err != nil {
		return nil, fmt.Errorf("cannot initialize mkvmerge: %w", err)
	}
	httpClient := &http.Client{}
	cmn := &common{
		httpClient: httpClient,
		merger:     merger,
	}
	channels := make([]sub.Channel, len(yamlConfig.Channels))
	for i, ch := range yamlConfig.Channels {
		matches := channelRegexp.FindStringSubmatch(ch.URL)
		if len(matches) == 0 {
			return nil, fmt.Errorf("channel %s: %q does not look like a youtube.com/channel/CHANNEL_ID_GOES_HERE URL", ch.Name, ch.URL)
		}
		channelID := sub.ChannelID(matches[1])
		qualityPolicy := sub.QualityStandard
		if ch.QualityPolicy != "" {
			qualityPolicy, err = sub.ParseQualityPolicy(ch.QualityPolicy)
			if err != nil {
				return nil, fmt.Errorf("channel %s: invalid quality policy: %w", ch.Name, err)
			}
		}
		active := true
		if ch.Active != nil {
			active = *ch.Active
		}
		channelFilter, err := filter.New(ch.Filter)
		if err != nil {
			return nil, fmt.Errorf("channel %s: invalid filter: %w", ch.Name, err)
		}
		channels[i] = &channel{
			BaseChannel: sub.NewBaseChannel(channelID, ch.Name, ch.MustWatch, active, ch.Tags, qualityPolicy, channelFilter.Filter),
			common:      cmn,
			uploadsPlaylistID: ch.UploadsPlaylistID,
		}
	}
	return channels, nil
}
