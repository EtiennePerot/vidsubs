package config

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"syscall"
	"text/template"
	"unicode"
	"unicode/utf8"

	"gopkg.in/yaml.v2"
	"perot.me/vidsubs/cache"
	"perot.me/vidsubs/filter"
	"perot.me/vidsubs/sub"
	"perot.me/vidsubs/yamlcfg"
	"perot.me/vidsubs/youtube"
)

type PathInfo struct {
	ConfigDir    string
	Channel      string
	Title        string
	ID           string
	YYYY, MM, DD string
	HHMMSS       string
	Extension    string
	MustWatch    string
}

type PathInfoWithTag struct {
	*PathInfo
	Tag string
}

var (
	samplePathInfo = &PathInfo{
		ConfigDir: "/foo/bar",
		Channel:   "my_channel",
		Title:     "My Little Video",
		ID:        "id=asdf",
		YYYY:      "2002",
		MM:        "06",
		DD:        "12",
		HHMMSS:    "144206",
		Extension: "mkv",
		MustWatch: "must_watch",
	}
	samplePathInfoWithTag = &PathInfoWithTag{
		PathInfo: samplePathInfo,
		Tag:      "my_tag",
	}
)

var (
	safeFilenameMultiUnderscores = regexp.MustCompile("_{2,}")
	safeFilenameWhitelist        = regexp.MustCompile("[^-_ ,.~!@#$%^&()+=\\[\\]{}'\"0-9a-zA-Z]")
)

func safeFilename(s string) string {
	if utf8.ValidString(s) {
		// Blacklist approach.
		var b strings.Builder
		b.Grow(len(s))
		for _, r := range s {
			if !unicode.IsPrint(r) {
				b.WriteRune('_')
			} else {
				switch r {
				case '\\', '`':
					b.WriteRune('_')
				case '/':
					b.WriteRune('-')
				case '"':
					b.WriteRune('\'')
				case ':':
					b.WriteString(" - ")
				case '|':
					b.WriteRune('-')
				case '?':
					b.WriteRune('？')
				case '*':
					b.WriteRune('＊')
				case '<':
					b.WriteRune('＜')
				case '>':
					b.WriteRune('＞')
				default:
					b.WriteRune(r)
				}
			}
		}
		return b.String()
	}
	// Whitelist approach.
	return safeFilenameWhitelist.ReplaceAllString(s, "_")
}

func cleanAndSafeFilename(s string) string {
	return safeFilenameMultiUnderscores.ReplaceAllString(safeFilename(s), "_")
}

func defaultDownloadOptions() *sub.DownloadOptions {
	return &sub.DownloadOptions{}
}

type SymlinkLayout struct {
	Name   string
	Layout *template.Template
	Filter *filter.Filter
	cfg *Config // Pointer back to parent config
}

func newSymlinkLayout(parent *Config, yamlCfg *yamlcfg.SymlinkLayout) (*SymlinkLayout, error) {
	symlinkLayout, err := template.New("symlink_layout").Parse(yamlCfg.Layout)
	if err != nil {
		return nil, fmt.Errorf("cannot parse symlink_layout %q (%q): %v", yamlCfg.Name, yamlCfg.Layout, err)
	}
	var b bytes.Buffer
	if err = symlinkLayout.Execute(&b, samplePathInfoWithTag); err != nil {
		return nil, fmt.Errorf("invalid storage_layout %q (%q): %v", yamlCfg.Name, yamlCfg.Layout, err)
	}
	f, err := filter.New(yamlCfg.Filter)
	if err != nil {
		return nil, fmt.Errorf("invalid filter: %v", err)
	}
	return &SymlinkLayout{
		Name: yamlCfg.Name,
		Layout: symlinkLayout,
		Filter: f,
		cfg:    parent,
	}, nil
}

// SymlinkPath returns the path that the symlink should be created at for the given video.
// Returns multiple paths e.g. if the video has multiple tags and the path uses tags.
// Returns no paths if the video doesn't pass the filter.
func (sl *SymlinkLayout) SymlinkPaths(video sub.Video, meta *sub.VideoMeta) ([]string, error) {
	if !sl.Filter.Filter(video, meta) {
		return nil, nil
	}
	pathInfos := sl.cfg.pathInfoWithTags(video, meta)
	pathsMap := make(map[string]struct{}, len(pathInfos))
	for _, pathInfo := range pathInfos {
		var b bytes.Buffer
		if err := sl.Layout.Execute(&b, pathInfo); err != nil {
			return nil, err
		}
		pathsMap[b.String()] = struct{}{}
	}
	paths := make([]string, 0, len(pathsMap))
	for path := range pathsMap {
		paths = append(paths, path)
	}
	sort.Strings(paths)
	return paths, nil
}

type Config struct {
	ConfigPath      string
	ConfigDir       string
	StorageLayout   *template.Template
	StorageDir      string
	StorageInfo     os.FileInfo
	SymlinkLayouts  []*SymlinkLayout
	DownloadOptions *sub.DownloadOptions
	Channels        []sub.Channel
	Cache           cache.Cache
}

func (cfg *Config) pathInfo(video sub.Video, meta *sub.VideoMeta) *PathInfo {
	channel := video.Channel()
	mustWatch := ""
	if channel.MustWatch() {
		mustWatch = "must_watch"
	}
	return &PathInfo{
		ConfigDir: cfg.ConfigDir,
		Channel:   cleanAndSafeFilename(channel.Name()),
		Title:     cleanAndSafeFilename(meta.Title),
		ID:        safeFilename(string(video.ID())),
		YYYY:      meta.Published.Format("2006"),
		MM:        meta.Published.Format("01"),
		DD:        meta.Published.Format("02"),
		HHMMSS:    meta.Published.Format("150405"),
		Extension: safeFilename(meta.Extension),
		MustWatch: mustWatch,
	}
}

func (cfg *Config) pathInfoWithTags(video sub.Video, meta *sub.VideoMeta) []*PathInfoWithTag {
	basePathInfo := cfg.pathInfo(video, meta)
	tags := video.Channel().Tags()
	if len(tags) == 0 {
		tags = []string{"untagged"}
	}
	pathInfos := make([]*PathInfoWithTag, 0, len(tags))
	for _, tag := range tags {
		pathInfos = append(pathInfos, &PathInfoWithTag{
			PathInfo: basePathInfo,
			Tag: tag,
		})
	}
	return pathInfos
}

func (cfg *Config) StoragePath(video sub.Video, meta *sub.VideoMeta) (string, error) {
	var b bytes.Buffer
	if err := cfg.StorageLayout.Execute(&b, cfg.pathInfo(video, meta)); err != nil {
		return "", err
	}
	layoutPath := b.String()
	path, err := filepath.Abs(layoutPath)
	if err != nil {
		return "", fmt.Errorf("cannot make video %v path %q absolute: %v", video, layoutPath, err)
	}
	return path, nil
}

func (cfg *Config) ChownToConfig(path string) error {
	if stat, ok := cfg.StorageInfo.Sys().(*syscall.Stat_t); ok {
		if err := os.Chown(path, int(stat.Uid), int(stat.Gid)); err != nil {
			return err
		}
	}
	return nil
}

func New(ctx context.Context, configPath string) (*Config, error) {
	configBytes, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, fmt.Errorf("cannot read config file %q: %v", configPath, err)
	}
	yamlCfg := &yamlcfg.Config{}
	if err := yaml.Unmarshal(configBytes, yamlCfg); err != nil {
		return nil, fmt.Errorf("cannot process config file %q: %v", configPath, err)
	}

	configDirRelative := filepath.Dir(configPath)
	configDir, err := filepath.Abs(configDirRelative)
	if err != nil {
		return nil, fmt.Errorf("cannot make config dir %q absolute: %v", configDirRelative, err)
	}
	samplePathInfo.ConfigDir = configDir
	var storageLayoutBuffer bytes.Buffer
	if !strings.Contains(yamlCfg.StorageLayout, "{{.ID}}") {
		return nil, fmt.Errorf("storage_layout %q must contain '{{.ID}}'", yamlCfg.StorageLayout)
	}
	storageLayout, err := template.New("storage_layout").Parse(yamlCfg.StorageLayout)
	if err != nil {
		return nil, fmt.Errorf("cannot parse storage_layout %q: %v", yamlCfg.StorageLayout, err)
	}
	if err = storageLayout.Execute(&storageLayoutBuffer, samplePathInfo); err != nil {
		return nil, fmt.Errorf("invalid storage_layout %q: %v", yamlCfg.StorageLayout, err)
	}
	var cacheFileBuffer bytes.Buffer
	cacheFileTemplate, err := template.New("cache_file").Parse(yamlCfg.CacheFile)
	if err != nil {
		return nil, fmt.Errorf("cannot parse cache_file %q: %v", yamlCfg.CacheFile, err)
	}
	if err = cacheFileTemplate.Execute(&cacheFileBuffer, samplePathInfo); err != nil {
		return nil, fmt.Errorf("invalid cache_file %q: %v", yamlCfg.CacheFile, err)
	}
	cacheFile := cacheFileBuffer.String()
	cacheDir := filepath.Dir(cacheFile)
	cacheDirInfo, err := os.Stat(cacheDir)
	if err != nil {
		return nil, fmt.Errorf("cache file is in invalid directory %q: %v", cacheDir, err)
	}
	if !cacheDirInfo.IsDir() {
		return nil, fmt.Errorf("cache file is in a non-directory parent %q", cacheDir)
	}
	configCache, err := cache.New(ctx, cacheFile)
	storageDir := strings.Replace(yamlCfg.StorageLayout, "{{.ConfigDir}}", configDir, -1)
	if index := strings.Index(storageDir, "{{"); index != -1 {
		storageDir = filepath.Clean(storageDir[0:index])
	}
	storageInfo, err := os.Stat(storageDir)
	if err != nil {
		return nil, fmt.Errorf("cannot check video storage directory %q: %v", storageDir, err)
	}
	if !storageInfo.IsDir() {
		return nil, fmt.Errorf("video storage %q is not a directory", storageDir)
	}
	downloadOptions := defaultDownloadOptions()
	if yamlCfg.DownloadOptions != nil {
		if yamlCfg.DownloadOptions.Bandwidth != "" {
			downloadOptions.Bandwidth = yamlCfg.DownloadOptions.Bandwidth
		}
	}

	youtubeChannels, err := youtube.ParseConfig(yamlCfg.YouTube)
	if err != nil {
		return nil, fmt.Errorf("YouTube: %v", err)
	}

	cfg := &Config{
		ConfigPath:      configPath,
		ConfigDir:       configDir,
		StorageLayout:   storageLayout,
		StorageDir:      storageDir,
		StorageInfo:     storageInfo,
		DownloadOptions: downloadOptions,
		SymlinkLayouts:  nil, // Filled in below.
		Channels:        youtubeChannels,
		Cache:           configCache,
	}

	channelIDs := make(map[sub.ChannelID]struct{}, len(cfg.Channels))
	for _, ch := range cfg.Channels {
		chID := ch.ID()
		if _, alreadyExists := channelIDs[chID]; alreadyExists {
			return nil, fmt.Errorf("duplicate channel ID: %q", chID)
		}
		channelIDs[chID] = struct{}{}
	}
	channelNames := make(map[string]struct{}, len(cfg.Channels))
	for _, ch := range cfg.Channels {
		chName := ch.Name()
		if _, alreadyExists := channelNames[chName]; alreadyExists {
			return nil, fmt.Errorf("duplicate channel name: %q", chName)
		}
		channelNames[chName] = struct{}{}
	}

	symlinkLayouts := make([]*SymlinkLayout, 0, len(yamlCfg.SymlinkLayouts))
	for _, layout := range yamlCfg.SymlinkLayouts {
		symlinkLayout, err := newSymlinkLayout(cfg, layout)
		if err != nil {
			return nil, fmt.Errorf("cannot parse symlink_layouts: %v", err)
		}
		symlinkLayouts = append(symlinkLayouts, symlinkLayout)
	}
	cfg.SymlinkLayouts = symlinkLayouts

	return cfg, nil
}
