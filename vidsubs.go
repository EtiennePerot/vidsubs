package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"perot.me/vidsubs/config"
	"perot.me/vidsubs/updater"
)

var progName = filepath.Base(os.Args[0])

func usage() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", progName)
	fmt.Fprintf(os.Stderr, "  %s <path/to/subs.yaml>\n", progName)
	flag.PrintDefaults()
}

func main() {
	ctx := context.Background()
	log.SetFlags(0)
	log.SetPrefix(fmt.Sprintf("%s: ", progName))
	flag.Usage = usage
	flag.Parse()

	if flag.NArg() != 1 {
		usage()
		os.Exit(2)
	}
	cfg, err := config.New(ctx, flag.Arg(0))
	if err != nil {
		log.Fatalf("Config error: %v", err)
	}
	if err = updater.New(cfg).Update(ctx); err != nil {
		log.Fatalf("Error updating subscriptions: %v", err)
	}
}
