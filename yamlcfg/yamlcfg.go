package yamlcfg

type Filter struct {
	Invert     *bool     `yaml:"invert"`
	TitleRegex *string   `yaml:"title_regex"`
	MustWatch  *bool     `yaml:"must_watch"`
	MaxDaysOld *int      `yaml:"max_days_old"`
	Tag        *string   `yaml:"tag"`
	And        []*Filter `yaml:"and"`
	Or         []*Filter `yaml:"or"`
}

type YouTubeChannel struct {
	Name              string   `yaml:"name"`
	URL               string   `yaml:"url"`
	Tags              []string `yaml:"tags"`
	MustWatch         bool     `yaml:"must_watch"`
	Active            *bool    `yaml:"active"`
	UploadsPlaylistID string   `yaml:"uploads_playlist_id"`
	QualityPolicy     string   `yaml:"quality_policy"`
	Filter            *Filter  `yaml:"filter"`
}

type YouTube struct {
	Channels []*YouTubeChannel `yaml:"channels"`
}

type SymlinkLayout struct {
	Name   string  `yaml:"name"`
	Layout string  `yaml:"layout"`
	Filter *Filter `yaml:"filter"`
}

type DownloadOptions struct {
	Bandwidth      string   `yaml:"bandwidth"`
}

type Config struct {
	StorageLayout   string           `yaml:"storage_layout"`
	CacheFile       string           `yaml:"cache_file"`
	SymlinkLayouts  []*SymlinkLayout `yaml:"symlink_layouts"`
	DownloadOptions *DownloadOptions `yaml:"download_options"`
	YouTube         *YouTube         `yaml:"youtube"`
}
