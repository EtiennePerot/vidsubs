package mkvmerge

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

type IdentifyInfoContainer struct {
	Recognized bool `json:"recognized"`
	Supported  bool `json:"supported"`
}

type IdentifyInfoTrack struct {
	ID    int    `json:"id"`
	Type  string `json:"type"`
	Codec string `json:"codec"`
}

type IdentifyInfo struct {
	Container *IdentifyInfoContainer `json:"container"`
	Errors    []string               `json:"errors"`
	Tracks    []*IdentifyInfoTrack   `json:"tracks"`
}

type TrackID int

func (id TrackID) String() string {
	return fmt.Sprintf("%d", id)
}

type Track struct {
	ID    TrackID
	Type  string
	Codec string
}

type File struct {
	Path                     string
	VideoTracks, AudioTracks []*Track
}

type Merger struct {
	mkvMergePath string
}

func mkvMergeError(err error) error {
	if exitErr, ok := err.(*exec.ExitError); ok {
		return fmt.Errorf("mkvmerge: %v: %v", err, string(exitErr.Stderr))
	}
	return fmt.Errorf("mkvmerge: %v", err)
}

func (m *Merger) Identify(ctx context.Context, inputFile string) (*File, error) {
	output, err := exec.CommandContext(ctx, m.mkvMergePath, "-J", inputFile).Output()
	if err != nil {
		return nil, mkvMergeError(err)
	}
	var info IdentifyInfo
	if err := json.Unmarshal(output, &info); err != nil {
		return nil, fmt.Errorf("cannot decode mkvmerge output: %v", err)
	}
	if len(info.Errors) > 0 {
		return nil, fmt.Errorf("mkvmerge: %v", info.Errors)
	}
	if !info.Container.Supported {
		return nil, errors.New("mkvmerge: container not supported")
	}
	var videoTracks, audioTracks []*Track
	for _, t := range info.Tracks {
		track := &Track{
			ID:    TrackID(t.ID),
			Type:  t.Type,
			Codec: t.Codec,
		}
		switch t.Type {
		case "video":
			videoTracks = append(videoTracks, track)
		case "audio":
			audioTracks = append(audioTracks, track)
		}
	}
	return &File{
		Path:        inputFile,
		VideoTracks: videoTracks,
		AudioTracks: audioTracks,
	}, nil
}

type Input struct {
	File   *File
	// If specified, only include these specific tracks.
	// Otherwise, include all tracks.
	Tracks []*Track
}

func (input *Input) IncludeTrack(track *Track) bool {
	if len(input.Tracks) == 0 {
		return true
	}
	for _, included := range input.Tracks {
		if track.ID == included.ID && track.Type == included.Type {
			return true
		}
	}
	return false
}

func (input *Input) Args() []string {
	var args []string
	if len(input.File.VideoTracks) > 0 {
		var trackIDs []string
		for _, videoTrack := range input.File.VideoTracks {
			if input.IncludeTrack(videoTrack) {
				trackIDs = append(trackIDs, videoTrack.ID.String())
			} else {
				trackIDs = append(trackIDs, "!"+videoTrack.ID.String())
			}
		}
		args = append(args, "--video-tracks", strings.Join(trackIDs, ","))
	}
	if len(input.File.AudioTracks) > 0 {
		var trackIDs []string
		for _, audioTrack := range input.File.AudioTracks {
			if input.IncludeTrack(audioTrack) {
				trackIDs = append(trackIDs, audioTrack.ID.String())
			} else {
				trackIDs = append(trackIDs, "!"+audioTrack.ID.String())
			}
		}
		args = append(args, "--audio-tracks", strings.Join(trackIDs, ","))
	}
	return append(args, input.File.Path)
}

type Attachment struct {
	Name        string
	MIMEType    string
	Description string
	Path        string
}

func (attachment *Attachment) Args() []string {
	return []string{
		"--attachment-name", attachment.Name,
		"--attachment-mime-type", attachment.MIMEType,
		"--attachment-description", attachment.Description,
		"--attach-file", attachment.Path,
	}
}

func (m *Merger) Merge(ctx context.Context, title string, inputs []*Input, attachments []*Attachment, outputFile string) error {
	args := []string{
		"-q",
		"-o", outputFile,
		"--title", title,
		"--default-language", "eng",
		"--flush-on-close",
	}
	for _, input := range inputs {
		args = append(args, input.Args()...)
	}
	for _, attachment := range attachments {
		args = append(args, attachment.Args()...)
	}
	err := exec.CommandContext(ctx, m.mkvMergePath, args...).Run()
	if err != nil {
		// Remove output file if it exists to ensure we leave no partial output.
		os.Remove(outputFile)
		return mkvMergeError(err)
	}
	return nil
}

func New() (*Merger, error) {
	mkvMergePath, err := exec.LookPath("mkvmerge")
	if err != nil {
		return nil, fmt.Errorf("cannot find 'mkvmerge' binary: %v", err)
	}
	return &Merger{
		mkvMergePath: mkvMergePath,
	}, nil
}
