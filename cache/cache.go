package cache

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"sort"

	"gopkg.in/yaml.v2"
	"perot.me/vidsubs/sub"
)

type skippedIDData struct {
	VideoID   string `yaml:"video_id"`
	Reason    string `yaml:"reason"`
}

type lastSeenIDFromChannelData struct {
	ChannelID string `yaml:"channel_id"`
	VideoID   string `yaml:"video_id"`
}

type yamlCache struct {
	SkippedIDs             []*skippedIDData             `yaml:"skipped_ids"`
	LastSeenIDFromChannels []*lastSeenIDFromChannelData `yaml:"last_seen_id_from_channels"`
}

type SkippedIDs interface {
	// ShouldSkip returns a non-empty string if the video should be skipped.
	ShouldSkip(sub.VideoID) string
	Add(id sub.VideoID, reason string)
}

type skippedIDs map[sub.VideoID]string

func (si skippedIDs) ShouldSkip(id sub.VideoID) string {
	return si[id]
}

func (si skippedIDs) Add(id sub.VideoID, reason string) {
	si[id] = reason
}

type LastSeenIDFromChannels interface {
	// LastSeen returns the empty string if no video has been seen for the
	// given channel ID.
	LastSeen(sub.ChannelID) sub.VideoID
	SetLastSeen(sub.ChannelID, sub.VideoID)
}

type lastSeenIDFromChannels map[sub.ChannelID]sub.VideoID

func (l lastSeenIDFromChannels) LastSeen(v sub.ChannelID) sub.VideoID {
	return l[v]
}

func (l lastSeenIDFromChannels) SetLastSeen(channel sub.ChannelID, video sub.VideoID) {
	l[channel] = video
}


type Cache interface {
	SkippedIDs()             SkippedIDs
	LastSeenIDFromChannels() LastSeenIDFromChannels
	Save(context.Context)    error
}

type cache struct {
	path                   string
	initial                *yamlCache
	skippedIDs             skippedIDs
	lastSeenIDFromChannels lastSeenIDFromChannels
}

func New(ctx context.Context, path string) (Cache, error) {
	cacheBytes, err := ioutil.ReadFile(path)
	if os.IsNotExist(err) {
		cacheBytes = []byte("{}")
	} else if err != nil {
		return nil, fmt.Errorf("cannot read cache data: %w", err)
	}
	yamlData := &yamlCache{}
	if err := yaml.Unmarshal(cacheBytes, yamlData); err != nil {
		return nil, fmt.Errorf("cannot process config file %q: %w", path, err)
	}
	skipped := skippedIDs(make(map[sub.VideoID]string, len(yamlData.SkippedIDs)))
	for _, skippedID := range yamlData.SkippedIDs {
		skipped.Add(sub.VideoID(skippedID.VideoID), skippedID.Reason)
	}
	lastSeenIDFromChannels := lastSeenIDFromChannels(make(map[sub.ChannelID]sub.VideoID, len(yamlData.LastSeenIDFromChannels)))
	for _, lastSeen := range yamlData.LastSeenIDFromChannels {
		lastSeenIDFromChannels.SetLastSeen(sub.ChannelID(lastSeen.ChannelID), sub.VideoID(lastSeen.VideoID))
	}
	return &cache{
		path: path,
		initial: yamlData,
		skippedIDs: skipped,
		lastSeenIDFromChannels: lastSeenIDFromChannels,
	}, nil
}

func (c *cache) SkippedIDs() SkippedIDs {
	return c.skippedIDs
}

func (c *cache) LastSeenIDFromChannels() LastSeenIDFromChannels {
	return c.lastSeenIDFromChannels
}

func (c *cache) Save(ctx context.Context) error {
	orderedSkippedIDs := make([]*skippedIDData, 0, len(c.skippedIDs))
	for skippedID, reason := range c.skippedIDs {
		orderedSkippedIDs = append(orderedSkippedIDs, &skippedIDData{
			VideoID: string(skippedID),
			Reason:  reason,
		})
	}
	sort.Slice(orderedSkippedIDs, func(i, j int) bool {
		return orderedSkippedIDs[i].VideoID < orderedSkippedIDs[j].VideoID
	})
	orderedLastSeenIDFromChannels := make([]*lastSeenIDFromChannelData, 0, len(c.lastSeenIDFromChannels))
	for channelID, videoID := range c.lastSeenIDFromChannels {
		orderedLastSeenIDFromChannels = append(orderedLastSeenIDFromChannels, &lastSeenIDFromChannelData{
			ChannelID: string(channelID),
			VideoID:   string(videoID),
		})
	}
	sort.Slice(orderedLastSeenIDFromChannels, func(i, j int) bool {
		return orderedLastSeenIDFromChannels[i].ChannelID < orderedLastSeenIDFromChannels[j].ChannelID
	})
	finalCache := &yamlCache{
		SkippedIDs:             orderedSkippedIDs,
		LastSeenIDFromChannels: orderedLastSeenIDFromChannels,
	}
	if reflect.DeepEqual(c.initial, finalCache) {
		// Nothing to do.
		return nil
	}
	cacheBytes, err := yaml.Marshal(finalCache)
	if err != nil {
		return fmt.Errorf("cannot marshal cache data: %w", err)
	}
	if err := ioutil.WriteFile(c.path, cacheBytes, 0644); err != nil {
		return fmt.Errorf("cannot write to %q: %w", c.path, err)
	}
	return nil
}
