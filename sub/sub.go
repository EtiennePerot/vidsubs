package sub

import (
	"context"
	"fmt"
	"time"
)

type VideoID string

type VideoMeta struct {
	Title     string
	Published time.Time
	Extension string
}

type Video interface {
	Channel() Channel
	ID() VideoID
	// Meta retrieves metadata about the video.
	Meta(ctx context.Context) (*VideoMeta, error)
	// Download downloads a video to the given output file.
	// Meta is gauranteed to have been called before Download is called.
	Download(ctx context.Context, downloadOptions *DownloadOptions, qualityPolicy QualityPolicy, outputFile string) error
}

// VideoError can be optionally returned for video meta retrieval or download.
type VideoError struct {
	error
	// IsPermanentReason is non-empty if the error is going to persist if
	// the operation is retried. It should be a short, non-video-specific
	// string.
	IsPermanentReason string
}

func NewPermanentVideoError(err error, permanentReason string) error {
	return &VideoError{err, permanentReason}
}

type QualityPolicy string
const (
	QualityBest = QualityPolicy("best")
	QualityStandard = QualityPolicy("standard")
	QualityMedium = QualityPolicy("medium")
	QualityTrash = QualityPolicy("trash")
	QualityWorst = QualityPolicy("worst")
)

func ParseQualityPolicy(qualityPolicy string) (QualityPolicy, error) {
	switch qualityPolicy {
		case "", "standard":
			return QualityStandard, nil
		case "best":
			return QualityBest, nil
		case "medium":
			return QualityMedium, nil
		case "trash":
			return QualityTrash, nil
		case "worst":
			return QualityWorst, nil
		default:
			return "", fmt.Errorf("invalid quality policy: %q", qualityPolicy)
	}
}

type VideoFilter func(video Video, meta *VideoMeta) bool

func noVideoFilter(video Video, meta *VideoMeta) bool {
	return true
}

type ChannelID string

type Channel interface {
	ID() ChannelID
	Name() string
	MustWatch() bool
	Active() bool
	Tags() []string
	QualityPolicy() QualityPolicy
	Filter() VideoFilter
	// Videos retrieves the available videos on the channel.
	// `lastSeen` is the video ID of the most-recently-published,
	// successfully-downloaded video ID from this channel. If no video
	// has ever successfully been downloaded from this channel, it is the
	// empty string.
	// The function should not return an error if it is confident that it
	// has successfully listed all videos that are more recent than the
	// `lastSeen` video ID.
	Videos(ctx context.Context, lastSeen VideoID) ([]Video, error)
}

type DownloadOptions struct {
	Bandwidth string
}

type BaseChannel struct {
	id            ChannelID
	name          string
	mustWatch     bool
	active        bool
	tags          []string
	qualityPolicy QualityPolicy
	videoFilter   VideoFilter
}

func (s *BaseChannel) ID() ChannelID {
	return s.id
}

func (s *BaseChannel) Name() string {
	return s.name
}

func (s *BaseChannel) String() string {
	return fmt.Sprintf("%s %v", s.Name(), s.Tags())
}

func (s *BaseChannel) MustWatch() bool {
	return s.mustWatch
}

func (s *BaseChannel) Active() bool {
	return s.active
}

func (s *BaseChannel) Tags() []string {
	return s.tags
}

func (s *BaseChannel) QualityPolicy() QualityPolicy {
	return s.qualityPolicy
}

func (s *BaseChannel) Filter() VideoFilter {
	if s.videoFilter == nil {
		return noVideoFilter
	}
	return s.videoFilter
}

func NewBaseChannel(id ChannelID, name string, mustWatch, active bool, tags []string, qualityPolicy QualityPolicy, videoFilter VideoFilter) *BaseChannel {
	return &BaseChannel{id, name, mustWatch, active, tags, qualityPolicy, videoFilter}
}

type BaseVideo struct {
	id         VideoID
	channel    Channel
	knownTitle string
}

func (v *BaseVideo) String() string {
	if v.knownTitle != "" {
		return fmt.Sprintf("[%s] %s [%s]", v.channel.Name(), v.knownTitle, v.ID())
	}
	return fmt.Sprintf("[%s] [%s]", v.channel.Name(), v.ID())
}

func (v *BaseVideo) Channel() Channel {
	return v.channel
}

func (v *BaseVideo) ID() VideoID {
	return v.id
}

// SetKnownTitle sets an internal BaseVideo field for human-friendlier String()
// return value.
// Returns itself for chainability.
func (v *BaseVideo) SetKnownTitle(knownTitle string) *BaseVideo {
	v.knownTitle = knownTitle
	return v
}

// NewBaseVideo returns a new BaseVideo.
func NewBaseVideo(id VideoID, channel Channel) *BaseVideo {
	return &BaseVideo{
		id:         id,
		channel:    channel,
	}
}
